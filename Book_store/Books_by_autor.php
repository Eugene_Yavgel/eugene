<?php
//session_start();

require_once "WorkData/Books.php";
require_once "WorkData/Basket.php";
require_once "authorize.php";


$books=new Books();
if(isset($_GET['id_book'])) {
    $book_id = $_GET['id_book'];
    $books->FindById($book_id);
    $book=$books->GetOneAssoc();
    //var_dump($books);
}

 // КОРЗИНА ДЛЯ ПОКУПОК
if(isset($_POST['buy'])){
$basket = new Basket();
$buy = $_POST['buy'];
$user_id=$_SESSION['id'];
$book_id=$_POST['buy']['book_id'];
$quantity = $_POST['buy']['quantity'];

//проверяем нет ли такой книги в корзине
$par_string = "book_id=$book_id AND user_id=$user_id";
$basket ->FindByParam(0, $par_string);
$mas_is_book = $basket->GetOneAssoc();
if($mas_is_book['quantity'] > 0) {
    $id = $mas_is_book['id'];
    $new_quantity = $quantity + $mas_is_book['quantity'];
    $param = " quantity=$new_quantity ";
    $basket->UpdateById($id, $param);
} else {    
$basket->InsertBasket($user_id, $book_id, $quantity);
}

$books->FindById($book_id);
$book=$books->GetOneAssoc();
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="CSS/Style.css" />
        <title><?php echo $book["name"];?></title>
    </head>
    <body>  
         <?php            
           echo "<p><a href='index.php'>На главную<a/></p>";         
           echo"<img src=\"".$book['picture']."\" width=\"150\" height=\"180\">";
           echo "<h3 style=\"color:green\">".$book["name"]." </h3>";
           echo  "<h4 style=\"color:red\"> Цена: ".$book["price"]." </h4>";		  
           $imp_date = $book["imprint_date"];
           echo "<h5> Дата издания: " .$imp_date. "<h5><br />";       
                         
           // Корзина
           if(isset($_SESSION['user'])){
           echo"<form action='Books_by_autor.php' method='POST'>";
           echo"<label>Количество:</label><input type='number' name=buy[quantity] /><br>";
           echo"<input type='hidden' name=buy[book_id] value=$book_id>";
           echo"<input type='submit' value='Купить'>";
           echo"</form>";  
           }          
         ?>
    </body>
</html>