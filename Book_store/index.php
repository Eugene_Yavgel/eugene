<?php
session_start();

require_once "WorkData/Autor.php";
require_once "WorkData/Books.php";
require_once "WorkData/Widgets.php";
require_once "WorkData/Genre.php";
require_once "authorize.php";


$page = 1;
$search = "";
$autor_id = NULL;
$genre_id = NULL;
$book = new Books();
$books = array();
if(isset($_GET['page'])){
    $page = $_GET['page'];
}
$book->SelectAll($page);
$books=$book->GetAssoc();
$pages_num = $book->Get_pages_num();

$genre = new Genre();
$genre->SelectAll(0);
$mas = $genre->GetAssoc();
foreach ($mas as $value) {
    $genres[$value['id']] = $value['name'];
}

if (isset($_GET['autor_id']) && $_GET['autor_id'] != "") {
    $autor_id = $_GET['autor_id'];
    $books = $book->SelectBooksByAutor($page, $autor_id);
    $pages_num = $book->Get_pages_num_by_autor($autor_id);
} 

if(isset($_GET['search_value']) && $_GET['search_value'] != ""){
    $search = $_GET['search_value'];
    $books= $book->SelectAllBySearch($page, $search);
    $pages_num = $book->Get_pages_num($search);
}

if (isset($_GET['serch_genre_id']) && $_GET['serch_genre_id'] != "") {
    $genre_id = $_GET['serch_genre_id'];
    $books = $book->SelectBooksByGenre($page, $genre_id);
    $pages_num = $book->Get_pages_num_by_genre($genre_id);
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="CSS/Style.css" />
        <title></title>
    </head>
    <body>  
        
        <div id="search">
            <form method="GET" action="index.php">
                <p><a href="index.php" style="margin-right: 20px">Показать всё</a></p>
                <b>Поиск по названию</b>
                <input name="search_value" type="text">
            </form>            
        </div>
        
        <form method="get" action="index.php">
            <p><label>Поиск по жанру:</label><?php Widgets::select_key($genres, 'serch_genre_id', false, 'Выберите жанр книги')?>
                <input type="submit" value="Найти">
            </p>
        </form>
        
        <div class="leftdiv"><h2>Авторы книг:</h2>
            <ul>
        <?php
        $autor = new Autor();
        $autor->SelectAll(0);
        $autor_mas = $autor->getAssoc();
        
        foreach($autor_mas as $aut) {
          $aut_id = $aut['id'];
          // Синтаксис запроса:
          // <a href='имя_файла?ключ1=значение1&ключ2=значение2&ключ3=значение3'>
	  echo "<li><a href='index.php?autor_id=$aut_id'>".$aut['full_name']."</a></li>";
	}
        ?>
            </ul>
        </div>
        
        <div class="rightdiv">
            Страница:
            <?php 
            for($i=1; $i<=$pages_num; $i++) {
                if($i == $page) {
                    echo "<b>$i</b>&nbsp;";
                } else {
                    echo "<a href='index.php?page=$i&search_value=".$search."&autor_id=".$autor_id."&serch_genre_id=".$genre_id."'>$i</a>&nbsp;";
                }
            }
            $search = '';
            $autor_id = NULL;
            $genre_id = NULL;
            ?>
        <form class="books" name="books">
        <h3>Название книг:</h3>    
       <table style= "font-size:12pt" cellpadding='5px' cellspacing='10px'>
         <?php
             if($books!=null){
                 $i=0;
                    foreach($books as $book){
                       if($i%4==0 || $i==0){
                           echo "<tr>";
                       }
                       $id_book = $book["id"];
                       $book_name = $book["name"];  
                       echo "<td><div style='background-color:DarkSlateGray; border:1px solid white; width:120px; height:170px; padding:5px; text-align: center'><img src='".$book['picture']."' height='80'><p><a href='Books_by_autor.php?id_book=$id_book' style='font-size:11pt'>".$book_name."</a></p><p style='color:darkorange; font-size:9pt'>Цена: ".$book['price']. " грн</p></div></td>";
                       $i++;
                       if($i%4==0){
                           echo "</tr>";
                       }                  
                    }                    
         ?>
              </table>
           </form>         
         <?php          
              }	
             else echo "На данный момент в этой  категории товаров нет";            
        ?>
      </div>
    </body>
</html>

<?php 
if(isset($_SESSION['user'])){
    echo "<script type='text/javascript'>form1.style.display='none'</script>";
    echo "<script type='text/javascript'>form2.style.display='block'</script>";    
    
    $user = new User();
    $login=$_SESSION['user'];
    $id=$user->FindIdByLog($login);
    $_SESSION['id']=$id[0];    
}
?>