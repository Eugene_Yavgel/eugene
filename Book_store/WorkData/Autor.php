<?php
require_once "Data.php";

//класс для работы с таблицей autor
class Autor extends Data {
    private $last_name;
    private $first_name;
    private $patronymic;
          
    function __construct() {
        parent::__construct();
        $this->table_name="autor";
        $this->last_name="last_name";
        $this->first_name="first_name";
        $this->patronymic="patronymic";
    }
    
    function FindAutor($autor_id){
        $pole = " full_name ";
        $par_string = " id=$autor_id ";
        $this->FindByParam(0, $par_string, $pole);
        $a = $this->GetOneAssoc();
        return $a['full_name'];
    }
}
?>