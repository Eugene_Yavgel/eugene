<?php
require_once "Data.php";

//класс для работы с таблицей book
class User extends Data {
    private $user_id;
    private $login;
    private $password;
    private $first_name;
    private $last_name;
    private $email;
    private $phone;
      
    function __construct() {
        parent::__construct();
        $this->table_name="user";
        $this->user_id="id";
        $this->login="login";
        $this->password="password";
        $this->first_name="first_name";
        $this->last_name="last_name";
        $this->email="email";
        $this->phone="phone";
    }
    
    function FindUser($log, $pas){
        $poles=" count(*) ";
        $par_string=" `login`=\"$log\" and `password`=\"$pas\" ";
        $this->FindByParam(0, $par_string, $poles);
        return $this->GetOneRow();
    }
    
    function InsertUser($log, $pas, $name, $fam, $em, $ph){
        $poles="`login`, `password`, `last_name`, `first_name`, `email`, `phone`";
        $values=" \"$log\", \"$pas\", \"$name\", \"$fam\", \"$em\", \"$ph\" ";
        return $this->Insert($poles, $values);        
    }
    
    function FindIdByLog($log){
    $par_string=" login=\"$log\"";
    $pole = " id";
    $this->FindByParam(0, $par_string, $pole);
    return $this->GetOneRow();     
    }    
}
    ?>