<?php
require_once "Data.php";

class Genre extends Data {
    private $name;
    
    function __construct() {
        parent::__construct();
        $this->table_name = "genre";
        $this->name = "name";
    }
    
    function FindGenre($genre_id){
        $pole = " name ";
        $par_string = " id=$genre_id ";
        $this->FindByParam(0, $par_string, $pole);
        $g = $this->GetOneAssoc();
        return $g['name'];
    }
}
?>