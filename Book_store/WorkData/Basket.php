<?php
require_once "Data.php";

//класс для работы с таблицей basket
class Basket extends Data {
    private $user_id;
    private $book_id;
    private $quantity;
    
    function __construct() {
        parent::__construct();
        $this->table_name="basket";
        $this->user_id="user_id";
        $this->book_id="book_id";
        $this->quantity="quantity";
    }
    
    function InsertBasket($user_id, $book_id, $quantity){
    $poles= "`user_id`, `book_id`, `quantity`";
    $values= "$user_id, $book_id, $quantity";
            
    $this->Insert($poles, $values);    
    }
    
    function FindByUser($user_id){
        $pole = "basket.id, basket.quantity, book.name, book.price, autor.full_name ";
        $join = " (book join autor on book.autor_id=autor.id) on book.id=basket.book_id ";
        $par_string = " basket.user_id=$user_id ";
        $this->FindByParam(0, $par_string, $pole, $join);
        return $this->GetAssoc();
    }
}

?>

