<?php
require_once "Data.php";

//класс для работы с таблицей book
class Books extends Data {
    private $books_id;
    private $name;
    private $price;
    private $imprint_date;
      
    function __construct() {
        parent::__construct();
        $this->table_name="book";
        $this->books_id="id";
        $this->name="name";
        $this->price="price";
        $this->imprint_date="imprint_date";
    }
    
    function SelectBooksByAutor($page, $autor_id) {
        $pole="name, id, price, picture";
        $par_string=" autor_id=\"".$autor_id."\"";
        $this->FindByParam($page, $par_string, $pole);
        return $this->GetAssoc();
    }
    
    function InsertBook($name,$price,$imp_date,$autor_id,$genre_id,$publ_id,$path_db){
    $poles= "`name`, `price`, `imprint_date`, `autor_id`, `genre_id`, `publisher_id`, `picture` ";
    $values= "\"$name\",\"$price\",\"$imp_date\",\"$autor_id\",\"$genre_id\",\"$publ_id\",\"$path_db\"";
            
    $this->Insert($poles, $values);    
    }
    
    function EditBook($edit, $picture){
       if($picture['tmp_name']!=""){
       $path="images/".$picture['name'];
    
    // Первый параметр - имя временного файла;
    //Второй - путь, куда загружается картинка.
    move_uploaded_file($picture['tmp_name'], $path);
       }
       else{
           $path=$edit['picture'];
       }
    $param = " name=\"$edit[name]\", price=$edit[price], imprint_date=$edit[imprint_date], autor_id=$edit[autor_id], genre_id=$edit[genre_id], publisher_id=$edit[publisher_id], picture=\"$path\" ";
    //echo $param;
    $this->UpdateById($edit['id'], $param);
    }
    
    function SelectAllBySearch($page=1, $search){
       $par_string = " name LIKE '%$search%'";
       $this->FindByParam($page, $par_string);
       return $this->GetAssoc();
    }
    
    //будет использ-ся в др ф-циях
    function dop($par_string, $poles) {
        $this->FindByParam(0, $par_string, $poles);
        $mas = $this->GetOneRow();
        //var_dump($mas);
        $books_count = $mas[0];
        //echo $books_count;
        return ceil($books_count/20);
    }   
    
    //считуем общ кол-во записей и опредиляем сколько из них получится стр
    function Get_pages_num($serch='') {
        $poles = " count(*) ";
        $par_string = " 1";
        
        if($serch != "") {
            $par_string = " name LIKE '%$serch%' ";
        }
        return $this->dop($par_string, $poles);
    }
    
    function Get_pages_num_by_autor($autor_id) {
        $poles = " count(*) ";
        $par_string = " autor_id=\"".$autor_id."\" ";
        
        return $this->dop($par_string, $poles);
    }
    
    function SelectBooksByGenre($page, $genre_id) {
        $pole="name, id, price, picture";
        $par_string=" genre_id=\"".$genre_id."\"";
        $this->FindByParam($page, $par_string, $pole);
        return $this->GetAssoc();
    }
    
    function Get_pages_num_by_genre($genre_id) {
        $poles = " count(*) ";
        $par_string = " genre_id=\"".$genre_id."\" ";
        
        return $this->dop($par_string, $poles);
    }
    
}
?>