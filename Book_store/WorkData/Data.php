<?php
include_once 'Config1.php';

//класс(общий) для работы с базой данных
class Data extends Config {
    protected $table_name;
    protected $query;
    protected $link;
    
    function __construct() {
        parent:: __construct();
    }
    
     //выбрать все
     function SelectAll($page=1, $pole="*", $join="") {
         $this->query="SELECT $pole FROM `$this->table_name`";
          if ($join!="") { 
            $this->query .= " JOIN $join ";      
        }
        if($page != 0){
            $min = 20 * ($page-1);
            $max = 20;
            $this->query .= "LIMIT ". $min . ", " . $max;
        }
     }
        
     //получение ассоциативного массива по результату работы запроса
     function GetAssoc(){
       $result = mysqli_query($this->link, $this->query);
        $selection=array();
        while ($mas = mysqli_fetch_assoc($result)){
            $selection[]=$mas;
        }
        return $selection;
    }
    
    //если результатом запроса является одна запись(асоц)
    function GetOneAssoc() {
     $result = mysqli_query($this->link,$this->query);

     $selection = mysqli_fetch_assoc($result);	
     return $selection;
    }
    
    function FindByParam($page=1, $par_string, $pole="*", $join="") {
         $this->query = "SELECT $pole FROM  `$this->table_name`";
         if ($join!=""){
             $this->query .= " JOIN  $join ";
         }
         $this->query .= " WHERE $par_string";
         if($page != 0){
            $min = 20 * ($page-1);
            $max = 20;
            $this->query .= "LIMIT ". $min . ", " . $max;
        }
        //echo $this->query;
    }
    
    function FindById ($id, $pole="*", $join="") {
        $this->query = "SELECT $pole FROM  `$this->table_name`";
         if ($join!=""){
             $this->query .= " JOIN  $join ";
         }
         $this->query .= " WHERE id=$id";
                  //echo $this->query;
    }
    
    //редактирование записи по ID
    function UpdateById($id, $param){
      //echo "UPDATE `$this->table_name` SET $param WHERE id=$id";
       mysqli_query($this->link, "UPDATE `$this->table_name` SET $param WHERE id=$id" );
    }
    
    //удаление по ID
    function DelById($id){
        //echo"DELETE FROM `$this->table_name`  WHERE id=$id ";
       mysqli_query($this->link, "DELETE FROM `$this->table_name`  WHERE id=$id ");
    }
    
    function Insert($poles,$values){
        $this->query="INSERT INTO `$this->table_name`($poles) VALUES($values)";
        //echo $this->query;
        mysqli_query($this->link, $this->query);  
        if ($this->table_name=="user"||$this->table_name=="order"){
            return mysqli_insert_id($this->link);
        }        
    }
    
    function GetOneRow() {
     $result = mysqli_query($this->link,$this->query);

     $selection = mysqli_fetch_row($result);	
     return $selection;
    }
    
}
?>