<?php
require_once "Data.php";

class Publisher extends Data {
        
    function __construct() {
        parent::__construct();
        $this->table_name = "publisher";
    }
    
    function FindPublisher($publisher_id){
        $pole = " name ";
        $par_string = " id=$publisher_id ";
        $this->FindByParam(0, $par_string, $pole);
        $p = $this->GetOneAssoc();
        return $p['name'];
    }
}
?>