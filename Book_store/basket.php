<?php
session_start();

require_once "WorkData/User.php";
require_once "WorkData/Basket.php";
require_once "WorkData/Order.php";
require_once "WorkData/Product_order.php";

$user_log = $_SESSION['user'];
$user = new User();
$par_string = " login = '$user_log'";
$user->FindByParam(0, $par_string);
$res=$user->GetOneAssoc();
$user_id=$res['id'];
//echo $user_id;
$basket = new Basket();
$bask_user=$basket->FindByUser($user_id);
//var_dump($bask_user);
if(isset($_POST['del_id'])){
    $del = $_POST['del_id'];    
    $basket->DelById($del);
    echo "Запись удалена";
}

if(isset($_POST['order_flag'])){
        $poles = "basket.id, basket.book_id, book.name, autor.full_name, book.price, basket.quantity ";
        $join = " (book JOIN autor ON book.autor_id = autor.id) ON basket.book_id = book.id ";
        $par_string = " user_id=$user_id";
        $basket->FindByParam(0, $par_string, $poles, $join);
        $info_order=$basket->GetAssoc();//вся информация о корзине данного клиента
        $sum = 0;
        $kol = 0;
        
        foreach ($info_order as $value) {
            $sum +=$value['price'] * $value['quantity'];
            $kol += $value['quantity'];
        }
                
        $order = new Order();
        $order_id =  $order->createOrder($user_id);
        //echo  $order_id ;
        $prod_ord = new ProductOrder();
        $flag = $prod_ord->dopByOrder($order_id, $info_order);
        if($flag==true){
            foreach ($info_order as $value) {
                $basket->DelById($value['id']);
            }
        }
        
        $mes = "Благодарим за посещение нашего сайта \n"
                . "Ваш заказ: \n";
        foreach ($info_order as $value) {
            $mes .= $value['fuul_name'] .  "\"".$value['name']. "\" - ". $value['quantity']." шт. по цене - " . $value['price']. "грн за единицу\n ";
        }
        $mes .= "Итого:" . $kol . "ед.\n  cумма к оплате" . $sum. "грн.";
        $to = $res['email'];
        $from = "books-store.ua";
        $subject = "Уведомление о покупке";
        $subject = "=?utf-8?=B?". base64_encode($subject)."?=";
        $headers = "from:$from\r\n reply-to:$from\r\n Content-type:text/plain; charset=utf-8\r\n";
        mail($to, $subject, $mes, $headers);
        header("location:basket.php?page=0&kol=$kol&sum=$sum");        
}
require_once "authorize.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="CSS/Style.css" />
        <title></title>
    </head>
    <body> 
        <div class="rightdiv" style="margin-top:7%">
            <form method="POST">
                <input type="hidden" name="order_flag" value="1">
                <input type="submit" value="Оформить заказ">
            </form><br>
            
            <table border="1" align="left">
                <tr><th>Название книги</th><th>Автор</th><th>Цена</th><th>Количество</th><th>Действие</th></tr>
                <?php
                //var_dump($bask_user);
                if ($bask_user!=null){
                    foreach ($bask_user as $value) {
                        echo "<tr>";
                        echo "<td>" . $value['name'] . "</td>";
                        echo "<td>" . $value['full_name'] . "</td>";
                        echo "<td>" . $value['price'] . "</td>";
                        echo "<td>" . $value['quantity'] . "</td>";
                        echo "<td><button type='button' onclick='del_from_basket(" . $value['id'] . ", this)'>Удалить</button></td>";
                        echo "</tr>";                   
                    }
                }
                ?>
            </table>
            <p id="info_order" style="text-align: center;"></p>
            
            <script src="js/jquery.js"></script>
            <script src="js/myjs.js"></script>
            <?php
            if(isset($_GET['kol']) && $_GET['kol'] != 0) {
                $kol = $_GET['kol'];
                $sum = $_GET['sum'];
                echo '<script type="text/javascript"> '
                . '$("#info_order").html("<br>Спасибо за покупку!<br> Ваш заказ принят.<br> Вы заказали '. $kol. ' ед. товара. На сумму '.$sum.' грн.")</script>';
            }
            ?>
        </div>       
    </body>
</html>