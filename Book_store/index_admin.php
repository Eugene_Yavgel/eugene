<?php
session_start();

require_once "WorkData/Autor.php";
require_once "WorkData/Books.php";
require_once "WorkData/Genre.php";
require_once "WorkData/Publisher.php";
require_once "WorkData/Widgets.php";

$page = 1;
$book = new Books();
if(isset($_GET['page'])) {
    $page = $_GET['page'];
}
 $book->SelectAll($page);
 $temp = $book->GetAssoc();
    $pages_num = $book->Get_pages_num();
    $autor = new Autor();
    $genre = new Genre();
    $publisher = new Publisher();
    
    foreach ($temp as $element){
        $element['autor_id'] = $autor->FindAutor($element['autor_id']);
        $element['genre_id'] = $genre->FindGenre($element['genre_id']);
        $element['publisher_id'] = $publisher->FindPublisher($element['publisher_id']);
        
        $books['all_info'][] = $element;       
    }
    
//Формируем выборку про авторов, жанров и издательств
$autor->SelectAll(0);
$a = $autor->GetAssoc();
foreach ($a as $v) {
    $books['autor_full_name'][$v['id']] = $v['full_name'];
}

$genre->SelectAll(0);
$g = $genre->GetAssoc();
foreach ($g as $v) {
    $books['genre_name'][$v['id']] = $v['name'];
}

$publisher->SelectAll(0);
$p = $publisher->GetAssoc();
foreach ($p as $v) {
    $books['publisher_name'][$v['id']] = $v['name'];
}

$flag_edit = 0;

if(isset($_GET['edit']) && $_GET['edit']==1){
    $flag_edit = $_GET['edit_id'];
}

if(isset($_POST['data_edit']) && isset($_FILES['picture'])) {
    $picture = $_FILES['picture'];
    $edit = $_POST['data_edit'];
    //var_dump ($picture);
    //var_dump($edit);  
 $book -> EditBook($edit, $picture);
 header("location:index_admin.php?page=$page");
 }

//обработчик для формы добавления нов книги
if(isset($_FILES['picture'])&&isset($_POST['add_book'])){
    if($_FILES['picture']['tmp_name']!=""){
    $path="images/".$_FILES['picture']['name'];
    
    // Первый параметр - имя временного файла;
    //Второй - путь, куда загружается картинка.
    move_uploaded_file($_FILES['picture']['tmp_name'], $path);
    $path_db="images/".$_FILES['picture']['name'];
    }
    else{
    $path_db="images/image1.png";
    }
    $name=$_POST['add_book']['name'];
    $price=$_POST['add_book']['price'];
    $imp_date=$_POST['add_book']['imprint_date'];
    $autor_id=$_POST['add_book']['autor_id'];
    $genre_id=$_POST['add_book']['genre_id'];
    $publ_id=$_POST['add_book']['publisher_id'];
      
      $books = new Books();
      $books->InsertBook($name,$price,$imp_date,$autor_id,$genre_id,$publ_id,$path_db);
      header("location:index_admin.php?page=$page");
}
if(isset($_POST['del_id'])){
    $id=$_POST['del_id'];
    $book->DelById($id);
    echo "Delete";
}
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="CSS/Style.css" />
        <title></title>
    </head>
    <body>
        <p><a href='index.php'>На главную</a></p>                       
        <div>
            Страница:
            <?php 
            for($i=1; $i<=$pages_num; $i++) {
                if($i == $page) {
                    echo "<b>$i</b>&nbsp;";
                } else {
                    echo "<a href='index_admin.php?page=$i'>$i</a>&nbsp;";
                }
            }            
            ?>
            
            <form method="POST" action="" enctype="multipart/form-data">
                <table border="1" align="left" width="100%">
                    <caption>Редактирование существующих книг</caption>
                    <tr>
                        <th>Название книги</th>
                        <th>Цена</th>
                        <th>Дата</th>
                        <th>ФИО автора</th>
                        <th>Жанр</th>
                        <th>Издательство</th>
                        <th>Изображение</th>
                        <th colspan="2">Действие</th>
                    </tr>
     <?php
                    foreach ($books['all_info'] as $b) {
                        echo '<tr>';
                                                                      
                        if($flag_edit == $b['id']){
                         echo "<td><input type='text' value='".$b['name']."' name='data_edit[name]' /></td>";
                         echo "<td><input type='text' value='".$b['price']."' name='data_edit[price]' /></td>";
                         echo "<td><input type='text' value='".$b['imprint_date']."' name=data_edit[imprint_date] /></td>";
                         echo"<td>";
                         Widgets::select_key($books['autor_full_name'], 'data_edit[autor_id]', true, $b['autor_id']);
                         echo"</td>";
                         echo"<td>";
                         Widgets::select_key($books['genre_name'], 'data_edit[genre_id]', true, $b['genre_id']);
                         echo"</td>";
                         echo"<td>";
                         Widgets::select_key($books['publisher_name'], 'data_edit[publisher_id]', true, $b['publisher_id']);
                         echo"</td>";
                         echo "<td>"
                         . "<input type='hidden' value='".$b['picture']."' name='data_edit[picture]' />".$b['picture']
                         . "<input type='file' value='".$b['picture']."' name='picture' />"
                           . "</td>";
                         echo "<input type='hidden' value='".$b['id']."' name=data_edit[id] />";
                         echo "<td><a href='index_admin.php?page=$page'>Отмена</a></td>";
                         echo "<td><input type='submit' value='Редактировать' /></td>";
                         
                        }
                        else {
                         echo "<td>".$b['name']."</td>";
                         echo "<td>".$b['price']."</td>";
                         echo "<td>".$b['imprint_date']."</td>";
                         echo "<td>".$b['autor_id']."</td>";
                         echo "<td>".$b['genre_id']."</td>";
                         echo "<td>".$b['publisher_id']."</td>";
                         echo "<td>".$b['picture']."</td>";
                         
                         //echo "<td><a href='index_admin.php?del=1&id=".$b['id']."'>Удалить</a></td>";
                         echo "<td><button  type='button' onclick='del_book(".$b['id'].",this)'>delete</button></td>";
                         echo "<td><a href='index_admin.php?edit=1&edit_id=".$b['id']."&page=$page'>Редактировать</a></td>";
                        } 
                                           
                        echo '</tr>';
                    }
     ?>
                </table>
            </form> 
        </div>
                
        <!-- форма для добавления нов книги -->
        <div id="add">
            <form method="POST" enctype="multipart/form-data">
                <h3 align="center">Добавить книгу в магазин:</h3><hr>
            <p><label>Название:</label><input type="text" name="add_book[name]"></p>
            <p><label>Цена:</label><input type="text" name="add_book[price]"></p>
            <p><label>Год издания:</label><input type="text" name="add_book[imprint_date]"></p>
            <p><label>Автор:</label><?php Widgets::select_key($books['autor_full_name'], 'add_book[autor_id]', false,"выберите автора книги");?></p>
            <p><label>Жанр:</label><?php Widgets::select_key($books['genre_name'], 'add_book[genre_id]', false,"выберите жанр книги");?></p>
            <p><label>Издательство:</label><?php Widgets::select_key($books['publisher_name'], 'add_book[publisher_id]', false,"выберите издательство книги");?></p>
            <p><input type="file" name="picture"></p>
            <p style="text-align: center"><input type="submit" value="Добавить книгу"><p>
          </form>
        </div>
        <script src="js/jquery.js" ></script>
        <script src="js/delete.js" ></script>
    </body>
</html>