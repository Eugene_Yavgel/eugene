-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 09 2015 г., 18:06
-- Версия сервера: 5.6.16
-- Версия PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `book_store`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` varchar(30) CHARACTER SET latin1 NOT NULL,
  `pas` varchar(256) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `log`, `pas`) VALUES
(1, 'admin', '2d8cc94a8c8b5ca7400969c5b2e572c1');

-- --------------------------------------------------------

--
-- Структура таблицы `autor`
--

CREATE TABLE IF NOT EXISTS `autor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `autor`
--

INSERT INTO `autor` (`id`, `full_name`) VALUES
(1, 'Грин Александр Степанович'),
(2, 'Достоевский Фёдор Михайлович'),
(3, 'Евгенидис Джефри'),
(4, 'Желязны Роджер'),
(5, 'Зюскинд Патрик'),
(6, 'Ивин Александр Архипович'),
(7, 'Йохансен Рой'),
(8, 'Кафка Франц'),
(9, 'Лукьяненко Сергей Васильевич'),
(10, 'Мольер Жан-Батист'),
(11, 'Носов Николай Николаевич'),
(12, 'Остин Джейн'),
(13, 'Пастернак Борис Леонидович'),
(14, 'Ремарк Эрих Мария'),
(15, 'Сэлинджер Джером Дэвид'),
(16, 'Толкин Джон Рональд'),
(17, 'Уайльд Оскар'),
(18, 'Фрэнсис Дик'),
(19, 'Хемингуэй Эрнест Миллер'),
(20, 'Цветаева Марина Ивановна'),
(21, 'Чехов Антон Павлович'),
(22, 'Шекспир Уильям'),
(23, 'Эко Умберто'),
(24, 'Юдин Борис Петрович'),
(25, 'Якименко Константин Николаевич');

-- --------------------------------------------------------

--
-- Структура таблицы `basket`
--

CREATE TABLE IF NOT EXISTS `basket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5` (`user_id`),
  KEY `FK_8` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `price` double DEFAULT NULL,
  `imprint_date` year(4) DEFAULT NULL,
  `autor_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `picture` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2` (`autor_id`),
  KEY `FK_3` (`genre_id`),
  KEY `FK_4` (`publisher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Дамп данных таблицы `book`
--

INSERT INTO `book` (`id`, `name`, `price`, `imprint_date`, `autor_id`, `genre_id`, `publisher_id`, `picture`) VALUES
(1, 'Алые паруса', 135, 2014, 1, 1, 3, 'images/image4.png'),
(2, 'Братья Карамазовы', 250, 2012, 2, 4, 3, 'images/image1.png'),
(3, 'Средний пол', 235, 2009, 3, 4, 4, 'images/image1.png'),
(4, 'Хроники Амбера', 450, 2008, 4, 4, 1, 'images/image1.png'),
(5, 'Парфюмер', 95, 2011, 5, 4, 2, 'images/image1.png'),
(6, 'Логика', 185, 2007, 6, 1, 1, 'images/image1.png'),
(7, 'Детектор лжи', 155, 2010, 7, 4, 2, 'images/image1.png'),
(8, 'Процесс', 210, 2011, 8, 4, 3, 'images/image1.png'),
(9, 'Ночной дозор', 225, 2013, 9, 4, 1, 'images/image1.png'),
(10, 'Мещанин во дворянстве', 93, 2009, 10, 2, 3, 'images/image1.png'),
(11, 'Приключения Незнайки', 160, 2014, 11, 1, 2, 'images/image1.png'),
(12, 'Гордость и предубеждение', 220, 2013, 12, 4, 3, 'images/image1.png'),
(13, 'Доктор Живаго', 187, 2010, 13, 4, 4, 'images/image1.png'),
(14, 'Враг', 130, 2009, 14, 3, 2, 'images/image1.png'),
(15, 'Над пропастью во ржи', 215, 2014, 15, 4, 4, 'images/image1.png'),
(16, 'Властелин колец', 315, 2014, 16, 4, 4, 'images/image4.png'),
(17, 'Идеальный муж', 105, 2008, 17, 2, 3, 'images/image1.png'),
(18, 'Банкир', 170, 2014, 18, 4, 1, 'images/image1.png'),
(19, 'Старик и море', 185, 2011, 19, 1, 4, 'images/image1.png'),
(20, 'Каменный Ангел', 120, 2012, 20, 1, 3, 'images/image1.png'),
(21, 'Дядя Ваня', 175, 2015, 21, 2, 1, 'images/image1.png'),
(22, 'Эдуард III', 160, 2010, 22, 1, 1, 'images/image1.png'),
(23, 'Имя розы', 205, 2012, 23, 4, 2, 'images/image4.png'),
(24, 'Город, который сошел с ума', 137, 2011, 24, 4, 2, 'images/image1.png'),
(25, 'Десять тысяч', 144, 2009, 25, 4, 3, 'images/image1.png'),
(26, 'Продавец счастья', 58, 2012, 1, 3, 2, 'images/image1.png'),
(27, 'Преступление и наказание', 114, 2015, 2, 4, 3, 'images/image4.png'),
(28, 'Идиот', 125, 2014, 2, 4, 1, 'images/image1.png'),
(29, 'Бесы', 146, 2013, 2, 4, 1, 'images/image1.png'),
(30, 'Сторож склепа', 110, 2014, 8, 2, 3, 'images/image1.png'),
(31, 'Прощай, оружие!', 185, 2014, 19, 4, 1, 'images/image1.png'),
(32, 'По ком звонит колокол', 165, 2012, 19, 4, 4, 'images/image1.png'),
(33, 'Маятник Фуко', 205, 2015, 23, 4, 2, 'images/image1.png'),
(34, 'Парижское кладбище', 235, 2014, 23, 4, 2, 'images/image1.png'),
(35, 'Ромео и Джульетта', 118, 2011, 22, 6, 2, 'images/image1.png'),
(36, 'Гамлет', 140, 2011, 22, 6, 4, 'images/image1.png'),
(37, 'Король Лир', 160, 2010, 22, 6, 3, 'images/image1.png'),
(38, 'На ранних поездах', 85, 2008, 13, 5, 3, 'images/image1.png'),
(39, 'Второе рождение', 95, 2015, 13, 5, 2, 'images/image1.png'),
(40, 'Пленный дух', 125, 2013, 20, 5, 2, 'images/image1.png'),
(41, 'Земные приметы', 120, 2014, 20, 5, 4, 'images/image1.png'),
(42, 'Час души', 130, 2011, 20, 5, 1, 'images/image1.png'),
(43, 'Бедные люди', 145, 2013, 2, 4, 2, 'images/image1.png'),
(44, 'Униженные и оскорблённые', 120, 2012, 2, 4, 2, 'images/image1.png'),
(45, 'Игрок', 95, 2013, 2, 4, 2, 'images/image1.png'),
(46, 'Подросток', 110, 2012, 2, 4, 2, 'images/image1.png'),
(47, 'Двойник', 50, 2012, 2, 3, 2, 'images/image1.png'),
(48, 'Баудолино', 205, 2014, 23, 4, 2, 'images/image1.png'),
(49, 'Макбет', 85, 2014, 22, 6, 2, 'images/image1.png'),
(50, 'Отелло', 95, 2013, 22, 6, 3, 'images/image1.png'),
(51, 'Антоний и Клеопатра', 140, 2012, 22, 6, 1, 'images/image1.png'),
(53, 'Тимон Афинский', 135, 2012, 22, 2, 3, 'images/image1.png'),
(54, 'Троил и Крессида', 155, 2010, 22, 2, 1, 'images/image1.png'),
(57, 'Тит Андроник', 150, 2011, 22, 2, 4, 'images/image1.png'),
(58, 'Сильмариллион', 215, 2014, 16, 4, 3, 'images/image1.png');

-- --------------------------------------------------------

--
-- Структура таблицы `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `genre`
--

INSERT INTO `genre` (`id`, `name`) VALUES
(1, 'Повесть'),
(2, 'Пьеса'),
(3, 'Рассказ'),
(4, 'Роман'),
(5, 'Поэзия'),
(6, 'Трагедия');

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `user_id`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `product_order`
--

CREATE TABLE IF NOT EXISTS `product_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7` (`order_id`),
  KEY `FK_1` (`book_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `product_order`
--

INSERT INTO `product_order` (`id`, `order_id`, `book_id`, `quantity`) VALUES
(1, 1, 1, 10),
(2, 7, 1, 100),
(3, 8, 2, 111),
(4, 11, 1, 121),
(5, 12, 1, 22),
(6, 13, 1, 12),
(7, 14, 3, 1),
(8, 16, 2, 50),
(9, 18, 1, 111),
(10, 19, 1, 5),
(11, 20, 1, 10),
(12, 21, 1, 11),
(13, 23, 2, 100),
(14, 25, 1, 112),
(15, 26, 1, 112),
(16, 27, 4, 4),
(17, 28, 1, 14),
(18, 29, 1, 4),
(19, 29, 2, 10),
(20, 39, 1, 8),
(21, 39, 2, 12),
(22, 40, 2, 10),
(23, 41, 2, 5),
(24, 46, 6, 3),
(25, 47, 1, 5),
(26, 49, 1, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `publisher`
--

CREATE TABLE IF NOT EXISTS `publisher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `publisher`
--

INSERT INTO `publisher` (`id`, `name`) VALUES
(1, 'АСТ'),
(2, ' Питер '),
(3, 'София'),
(4, 'Эксмо');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqLogEm` (`login`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `last_name`, `first_name`, `email`, `phone`) VALUES
(1, 'Alex', '12345', 'Иванов', 'Александр', 'Alex@host.ua', '123-45-67'),
(2, 'Eugene', '440b083de0b459db38d15b3836b4996e', 'R', 'W', 'Cabul@forever.ua', '0681158525'),
(3, 'Alex', '65d852b7880fd77cab08ad04539b34cb', 'T', 'S', 'Alex@forever.ua', '123-23-78'),
(4, 'Anna', 'b5997c289137b58e5a5a0dc05a18ca7b', 'D', 'W', 'J@Q', '123456'),
(5, 'S', '65d852b7880fd77cab08ad04539b34cb', 'F', 'G', 'F@R', '123'),
(6, 'Alex', 'ddd1ae69b32df6f69aec5db30675901e', 'dddd', 'dddd', 'ddd@rrr', '7895421');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `basket`
--
ALTER TABLE `basket`
  ADD CONSTRAINT `FK_5` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_8` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `FK_2` FOREIGN KEY (`autor_id`) REFERENCES `autor` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_3` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_4` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_6` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_order`
--
ALTER TABLE `product_order`
  ADD CONSTRAINT `FK_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_7` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
